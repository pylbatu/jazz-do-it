﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PylBatu.JazzDoIt.Core.Models
{
    public class ToDo : Auditable
    {
        public Guid Id { get; private set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public ToDo() {}

        public ToDo(string title, string description) : base()
        {
            Id = Guid.NewGuid();
            Title = title;
            Description = description;
        }

        public void SetDetails(string title, string description)
        {
            Title = title;
            Description = description;
            LastDateModified = DateTime.Now;
        }

    }
}
