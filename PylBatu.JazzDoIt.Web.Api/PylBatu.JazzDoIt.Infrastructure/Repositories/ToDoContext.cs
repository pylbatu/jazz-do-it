﻿using Microsoft.EntityFrameworkCore;
using PylBatu.JazzDoIt.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace PylBatu.JazzDoIt.Infrastructure.Repositories
{
    public class ToDoContext : DbContext
    {
        public ToDoContext(DbContextOptions<ToDoContext> options)
            : base(options)
        {
        }

        public DbSet<ToDo> TodoItems { get; set; }
    }
}
