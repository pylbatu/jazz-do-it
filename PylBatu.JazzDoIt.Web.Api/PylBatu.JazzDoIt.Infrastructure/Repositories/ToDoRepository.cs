﻿using PylBatu.JazzDoIt.Core.Interfaces;
using PylBatu.JazzDoIt.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PylBatu.JazzDoIt.Infrastructure.Repositories
{
    public class ToDoRepository : IRepository<ToDo>
    {
        private readonly ToDoContext _todoContext;

        public ToDoRepository(ToDoContext toDoContext)
        {
            _todoContext = toDoContext;
        }

        public void Add(ToDo request)
        {
            _todoContext.Add(request);
            _todoContext.SaveChanges();
        }      

        public IEnumerable<ToDo> Get()
        {
            var result = _todoContext.TodoItems.ToList();
            return result;
        }

        public ToDo Get(Guid id)
        {
            var result =  _todoContext.TodoItems.Where(toGet => toGet.Id == id).FirstOrDefault();
            return result;
        }

        public void Update(ToDo request)
        {
            _todoContext.Entry(request).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            _todoContext.SaveChanges();
        }

         public void Delete(ToDo request)
        {
            _todoContext.Remove(request);
            _todoContext.SaveChanges();
        }
    }
}
