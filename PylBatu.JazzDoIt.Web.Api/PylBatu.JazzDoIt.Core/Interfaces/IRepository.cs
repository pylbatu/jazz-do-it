﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PylBatu.JazzDoIt.Core.Interfaces
{
    public interface IRepository<T> where T : class
    {
        void Add(T request);
        void Update(T request);
        void Delete(T request);
        IEnumerable<T> Get();

        T Get(Guid id);
    }
}
