﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PylBatu.JazzDoIt.Core.Models
{
    public abstract class Auditable
    {
        public DateTime DateCreated { get; private set; }

        public DateTime? LastDateModified { get; protected set; }

        public Auditable()
        {
            DateCreated = DateTime.Now;
        }
    }
}
