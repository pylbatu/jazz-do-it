﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PylBatu.JazzDoIt.Web.Api.Requests
{
    public class ToDoRequest
    {
        public string Title { get; set; }
        public string Description { get; set; }
    }
}
