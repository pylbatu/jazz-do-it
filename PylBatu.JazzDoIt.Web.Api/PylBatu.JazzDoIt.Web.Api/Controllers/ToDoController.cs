﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PylBatu.JazzDoIt.Core.Interfaces;
using PylBatu.JazzDoIt.Core.Models;
using PylBatu.JazzDoIt.Web.Api.Requests;

namespace PylBatu.JazzDoIt.Web.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ToDoController : ControllerBase
    {
        private readonly IToDoService _toDoService;

        public ToDoController(
                              IToDoService toDoService
                                )
        {
            _toDoService = toDoService;
        }

        [HttpGet]
        public async Task<IEnumerable<ToDo>> Get()
        {
            return await _toDoService.GetAllAsync();
        }

        [HttpGet("{id}", Name = "Get")]
        public async Task<ToDo> Get(Guid id)
        {
            return await _toDoService.GetAsync(id);
        }

        [HttpPost]
        public async Task Post([FromBody] ToDoRequest request)
        {
            var model = new ToDo(request.Title, request.Description);
            await _toDoService.AddAsync(model);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(Guid id, [FromBody] ToDo request)
        {
            try
            {
                await _toDoService.Update(id, request);

                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }

        [HttpDelete("{id}")]
        public async Task Delete(Guid id)
        {
            await _toDoService.DeleteAsync(id);
        }
    }
}
