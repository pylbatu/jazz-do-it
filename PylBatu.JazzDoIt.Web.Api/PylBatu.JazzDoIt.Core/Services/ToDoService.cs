﻿using PylBatu.JazzDoIt.Core.Interfaces;
using PylBatu.JazzDoIt.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

namespace PylBatu.JazzDoIt.Core.Services
{
    public class ToDoService : IToDoService
    {
        private readonly IRepository<ToDo> _repository;

        public ToDoService(IRepository<ToDo> repository)
        {
            _repository = repository;
        }

        public async Task AddAsync(ToDo model)
        {
            _repository.Add(model);
        }


        public async Task<IEnumerable<ToDo>> GetAllAsync()
        {
            return _repository.Get();
        }

        public async Task<ToDo> GetAsync(Guid id)
        {
            var item = _repository.Get(id);
            return item;
        }

        public async Task Update(Guid id, ToDo model)
        {
            var item = await GetAsync(id);

            if (item == null)
            {
                throw new ApplicationException("Item does not exist.");
            }

            item.SetDetails(model.Title, model.Description);

            _repository.Update(item);
        }
        public async Task DeleteAsync(Guid id)
        {
            var item = await GetAsync(id);
            _repository.Delete(item);
        }
    }
}
