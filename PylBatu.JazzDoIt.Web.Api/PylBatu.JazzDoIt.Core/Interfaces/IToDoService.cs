﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using PylBatu.JazzDoIt.Core.Models;

namespace PylBatu.JazzDoIt.Core.Interfaces
{
    public interface IToDoService
    {
        Task AddAsync(ToDo model);
        Task Update(Guid id, ToDo model);
        Task DeleteAsync(Guid id);
        Task<ToDo> GetAsync(Guid id);
        Task<IEnumerable<ToDo>> GetAllAsync();
    }
}
